OUTPUT ?= libutils.dll

CC := gcc
AR := ar
MKDIR := mkdir -p
RM := rm -rf
ECHO := echo
INSTALL := install

C_FILES :=
INCLUDE_DIRS :=
include sources.mk

buildtype ?= debug

SOURCE_DIR  := .
BUILD_DIR   := ../../build/windows/common/libutils/$(buildtype)
INSTALL_DIR := ../../bin/windows

C_OBJS := $(C_FILES:%.c=$(BUILD_DIR)/%.o)

ifeq ($(buildtype),release)
	CFLAGS := -O2 -Wall $(CFLAGS_MORE)
else
	CFLAGS := -O2 -Wall -g $(CFLAGS_MORE)
endif

ARFLAGS := rcs
LDFLAGS :=
LDLIBS  :=
DEPFLAGS = -MMD -MP -MF$(@:%.o=%.d) -MT$(@)

all: $(INSTALL_DIR)/$(OUTPUT)
	@$(ECHO) "Done!"

$(INSTALL_DIR)/$(OUTPUT): $(BUILD_DIR)/$(OUTPUT)
	$(MKDIR) $(dir $@)
	$(INSTALL) $^ $@
	
$(BUILD_DIR)/$(OUTPUT): $(C_OBJS)
	@$(ECHO) "Creating $@"
	@$(CC) -shared -o $@ $^

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c
	@$(MKDIR) $(dir $@)
	@$(ECHO) "CC $<"
	@$(CC) $(CFLAGS) $(DEPFLAGS) -c $< -o $@

clean:
	$(RM) $(BUILD_DIR)
	$(RM) $(INSTALL_DIR)/$(OUTPUT)